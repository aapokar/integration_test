const expect = require("chai").expect;
const converter = require("../src/converter");

describe('Color Code Converter', () => {
    describe("Hex to RGB conversion", function() {
        it("converts the basic colors", function() {
          var red   = converter.hexToRgb("ff0000");
          var green = converter.hexToRgb("00ff00");
          var blue  = converter.hexToRgb("0000ff");
    
          expect(red).to.deep.equal([255, 0, 0]);
          expect(green).to.deep.equal([0, 255, 0]);
          expect(blue).to.deep.equal([0, 0, 255]);
        });
      });
})