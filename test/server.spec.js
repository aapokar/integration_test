const expect = require('chai').expect;
const request = require('request');
const app = require('../src/server');
const port = 3000;

let server;

describe('Color Code Converter API', () => {
    before('Start server before running tests', (done) => {
        server = app.listen(port, () => {
            console.log('Runnin in port 3000')
            done();
        })
    })

    describe("Hex to RGB conversion", function() {
        var url = "http://localhost:3000/hexToRgb?hex=00ff00";
    
        it("returns status 200", function(done) {
          request(url, function(error, response, body) {
            expect(response.statusCode).to.equal(200);
            done();
          });
        });
    
        it("returns the color in RGB", function(done) {
          request(url, function(error, response, body) {
            expect(body).to.equal("[0,255,0]");
            done();
          });
        });
      });

    after('Stop server after tests', (done) => {
        server.close();
        done();
    })
})