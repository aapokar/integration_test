const express = require('express');
const converter = require('./converter');

const app = express();
const port = 3000;

app.get('/', (req, res) => res.send('Hello'))
app.get("/hexToRgb", function(req, res) {

    var hex = req.query.hex;
    var rgb = converter.hexToRgb(hex);
    res.send(JSON.stringify(rgb));
  
  });

if (process.env.NODE_ENV === 'test') {
    module.exports = app
} else {
    app.listen(port, () => console.log(`Server: localhost:${port}`))
}